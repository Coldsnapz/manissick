﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public float _MaxinfectableHuman { get; set; }
    private float _infectedHuman;
    public float INFECTEDHUMAN { get { return _infectedHuman; } set { _infectedHuman = value; } }

    public float _MaxInfectableObject { get; set; }
    private float _infectedObject;
    public float INFECTEDOBJECT { get { return _infectedObject; } set { _infectedObject = value; } }

    [SerializeField] float _MaxTime = 0;
    private float _time;
    public float TIME { get { return _time; } set { _time = value; } }

    public float _score;
    public float _scoreObject;
    public float _scoreHuman;
    public float _scoreTime;
    public float _scoreSuspect;
    public bool _enterArea { get; set; } = false;
    public bool _ActiveSuspectZoneAfterEnterArea;
    public bool _GuardStandbyAtArea { get; set; } = false;
    public bool _DeactiveSuspectZoneAfterEnterArea;
    public GameObject SuspectZone = null;
    public bool _escapeArea { get; set; } = false;
    private bool ENDED = false;
    public bool result { get; set; }
    public Text StatusText = null;
    public string Displaying_place_is_name = "somewhere in the world";
    public int chasedby { get; set; }
    public bool InSuspectZone { get; set; }

    private float _suspectPoint;
    public float SUSPECTPOINT { get { return _suspectPoint; } set { _suspectPoint = value; } }
    public float RegenerationCD { get; set; }

    [SerializeField] Text _timer = null;
    [SerializeField] public Slider _suspectBar = null;
    [SerializeField] Text _objectiveEnterArea = null;
    [SerializeField] Text _objectiveInfectedHuman = null;
    [SerializeField] Text _objectiveInfectedObject = null;
    [SerializeField] Text _objectiveEscapeArea = null;
    [SerializeField] string NextStage = null;
    [SerializeField] float NextUnlock;
    private bool IsPause()
    {
        if (Time.timeScale == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void Awake()
    {
        SingletonScore.Instance.ResetScore();
        _score = 0;
        _scoreObject = 0;
        _scoreHuman = 0;
        _scoreTime = 0;
        _scoreSuspect = 0;

        RegenerationCD = 0;
        _objectiveEnterArea.color = Color.white;
        _objectiveEnterArea.text = "Enter the area.";
        _objectiveInfectedHuman.color = Color.white;
        _objectiveInfectedHuman.text = "Infect people with Covid-19 : " + INFECTEDHUMAN + "/" + _MaxinfectableHuman;
        _objectiveInfectedObject.color = Color.white;
        _objectiveInfectedObject.text = "Infect object with Covid-19 : " + INFECTEDOBJECT + "/" + _MaxInfectableObject;
        _objectiveEscapeArea.color = Color.white;
        _objectiveEscapeArea.text = "Escape the area.";
        TIME = _MaxTime + 5;
        DisplayTime(TIME);
        SUSPECTPOINT = _suspectBar.maxValue;
        _suspectBar.value = SUSPECTPOINT;
    }
    private void Start()
    {
        Cursor.visible = false;
        if (SuspectZone != null)
        {
            if (_ActiveSuspectZoneAfterEnterArea)
            {
                SuspectZone.SetActive(false);
                _GuardStandbyAtArea = false;
            }
            else if (_DeactiveSuspectZoneAfterEnterArea)
            {
                SuspectZone.SetActive(true);
            }
            if (!_ActiveSuspectZoneAfterEnterArea)
            {
                _GuardStandbyAtArea = true;
            }
        }
    }
    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        _timer.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void Update()
    {
        if (_escapeArea)
        {
            StatusText.text = "";
        }
        else if (InSuspectZone && chasedby > 0)
        {
            StatusText.color = Color.red;
            StatusText.text = "You got chased by security guards, Leave this area!";
        }
        else if (chasedby > 0)
        {
            StatusText.color = Color.red;
            StatusText.text = "You got chased by security guards, run away!";
        }
        else if (InSuspectZone)
        {
            StatusText.color = Color.red;
            StatusText.text = "This area is too strict, leave the area!";
        }
        else if (_enterArea)
        {
            StatusText.color = Color.white;
            StatusText.text = "Spread the virus or find the exit.";
        }
        else
        {
            StatusText.color = Color.white;
            StatusText.text = "Find the way to get inside " + Displaying_place_is_name + ".";
        }

        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.E) && Input.GetKey(KeyCode.B) && Input.GetKey(KeyCode.U) && Input.GetKey(KeyCode.G))
        {
            Win();
        }
        if (Input.GetKeyDown(KeyCode.Escape) && !ENDED)
        {
            if (!IsPause())
            {
                Time.timeScale = 0;
                Cursor.visible = true;
                SceneManager.LoadScene("PauseScene", LoadSceneMode.Additive);
            }
        }
        if (SUSPECTPOINT < _suspectBar.maxValue)
        {
            if (RegenerationCD < 3)
            {
                RegenerationCD += Time.deltaTime;
            }
            else
            {
                SUSPECTPOINT += Time.deltaTime * 3;
                if (SUSPECTPOINT > _suspectBar.maxValue)
                {
                    SUSPECTPOINT = _suspectBar.maxValue;
                }
            }
        }

        if (INFECTEDHUMAN == _MaxinfectableHuman)
        {
            _objectiveInfectedHuman.color = Color.green;
            _objectiveInfectedHuman.text = "Infect people with Covid-19 : " + INFECTEDHUMAN + "/" + _MaxinfectableHuman + " \u2713";
        }
        else
        {
            _objectiveInfectedHuman.text = "Infect people with Covid-19 : " + INFECTEDHUMAN + "/" + _MaxinfectableHuman;
        }

        if (INFECTEDOBJECT == _MaxInfectableObject)
        {
            _objectiveInfectedObject.color = Color.green;
            _objectiveInfectedObject.text = "Infect object with Covid-19 : " + INFECTEDOBJECT + "/" + _MaxInfectableObject + " \u2713";
        }
        else
        {
            _objectiveInfectedObject.text = "Infect object with Covid-19 : " + INFECTEDOBJECT + "/" + _MaxInfectableObject;
        }


        if (_time > 0)
        {
            TIME -= Time.deltaTime;
        }
        else
        {
            if (!ENDED)
            {
                Lose();
                ENDED = true;
            }
        }
        DisplayTime(TIME);

        if (SUSPECTPOINT <= 0)
        {
            SUSPECTPOINT = 0;
            if (!ENDED)
            {
                Lose();
                ENDED = true;
            }
        }
        _suspectBar.value = SUSPECTPOINT;

        if (_enterArea && _escapeArea)
        {
            Win();
        }
    }

    public void AreaEnter()
    {
        _objectiveEnterArea.color = Color.green;
        _objectiveEnterArea.text = "Enter the area." + " \u2713";
        _enterArea = true;
        if (SuspectZone != null)
        {
            if (_ActiveSuspectZoneAfterEnterArea)
            {
                SuspectZone.SetActive(true);
                _GuardStandbyAtArea = true;
            }
            else if (_DeactiveSuspectZoneAfterEnterArea)
            {
                SuspectZone.SetActive(false);
            }
        }
    }
    public void EscapeArea()
    {
        _objectiveEscapeArea.color = Color.green;
        _objectiveEscapeArea.text = "Escape the area." + " \u2713";
        _escapeArea = true;
    }
    public void Lose()
    {
        Cursor.visible = true;
        scoreCulculation();
        if (!ENDED)
        {
            Time.timeScale = 0;
            SingletonScore.Instance.Result = false;
            SingletonScore.Instance.Score = _score;
            SingletonScore.Instance.Object = INFECTEDOBJECT.ToString() + "/" + _MaxInfectableObject.ToString();
            SingletonScore.Instance.Human = INFECTEDHUMAN.ToString() + "/" + _MaxinfectableHuman.ToString();
            SingletonScore.Instance.TimeLeft = _timer.text;
            SingletonScore.Instance.Suspect = _suspectBar.value;
            SingletonScore.Instance.CurrentStage = SceneManager.GetActiveScene().name;
            SingletonScore.Instance.NextStage = null;
            ENDED = true;
            SceneManager.LoadScene("ResultScene", LoadSceneMode.Additive);
        }
    }
    public void Win()
    {
        Cursor.visible = true;
        scoreCulculation();
        if (!ENDED)
        {
            Time.timeScale = 0;
            SingletonScore.Instance.Result = true;
            SingletonScore.Instance.Score = _score;
            SingletonScore.Instance.Object = INFECTEDOBJECT.ToString() + "/" + _MaxInfectableObject.ToString();
            SingletonScore.Instance.Human = INFECTEDHUMAN.ToString() + "/" + _MaxinfectableHuman.ToString();
            SingletonScore.Instance.TimeLeft = _timer.text;
            SingletonScore.Instance.Suspect = _suspectBar.value;
            SingletonScore.Instance.CurrentStage = SceneManager.GetActiveScene().name;
            if (NextUnlock == 2)
            {
                SingletonScore.Instance.Stage2 = true;
            }
            else if (NextUnlock == 3)
            {
                SingletonScore.Instance.Stage2 = false;
            }

            if (NextStage != null)
            {
                SingletonScore.Instance.NextStage = NextStage;
            }
            else
            {
                SingletonScore.Instance.NextStage = "SceneMainMenuBackground";
            }
            ENDED = true;
            SceneManager.LoadScene("ResultScene", LoadSceneMode.Additive);
        }
    }
    public void scoreCulculation()
    {
        if (_MaxinfectableHuman == 0)
        {
            _MaxinfectableHuman = 1;
            INFECTEDHUMAN = _MaxinfectableHuman;
        }
        if (_MaxInfectableObject == 0)
        {
            _MaxInfectableObject = 1;
            INFECTEDOBJECT = _MaxInfectableObject;
        }

        _scoreObject = (INFECTEDOBJECT / _MaxInfectableObject) * 150;
        _scoreHuman = (INFECTEDHUMAN / _MaxinfectableHuman) * 250;
        _scoreTime = (TIME / _MaxTime) * 30;
        _scoreSuspect = (_suspectBar.value / _suspectBar.maxValue) * 12.5f;

        _score = _scoreObject + _scoreHuman + _scoreTime + _scoreSuspect;
    }
}
