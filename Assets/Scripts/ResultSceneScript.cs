﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultSceneScript : MonoBehaviour
{
    [SerializeField] public Texture win;
    [SerializeField] public Texture lose;
    [SerializeField] public RawImage Result;
    [SerializeField] public Text Score;
    [SerializeField] public Text Object;
    [SerializeField] public Text Human;
    [SerializeField] public Text TimeLeft;
    [SerializeField] public Text Suspect;
    [SerializeField] public Button NextButton;
    private string CurrentStage = "";
    private string NextStage = "";

    void Start()
    {
        if (SingletonScore.Instance.Result)
        {
            Result.texture = win;
        }
        else
        {
            Result.texture = lose;
        }
        Score.text = SingletonScore.Instance.Score.ToString();
        Object.text = SingletonScore.Instance.Object.ToString();
        Human.text = SingletonScore.Instance.Human.ToString();
        TimeLeft.text = SingletonScore.Instance.TimeLeft.ToString();
        Suspect.text = SingletonScore.Instance.Suspect.ToString();
        CurrentStage = SingletonScore.Instance.CurrentStage;
        NextStage = SingletonScore.Instance.NextStage;
        if (NextStage == null)
        {
            NextButton.interactable = false;
        }
        SingletonScore.Instance.SaveFile();
    }
    public void LoadStageSelect()
    {
        Time.timeScale = 1;
        SceneManagementSingleton.Instance.isQuitingStage = true;
        SceneManager.UnloadSceneAsync("ResultScene");
        SceneManager.LoadScene("SceneMainMenuBackground", LoadSceneMode.Single);
    }
    public void LoadCurrentStage()
    {
        Time.timeScale = 1;
        SceneManager.UnloadSceneAsync("ResultScene");
        SceneManager.LoadScene(CurrentStage, LoadSceneMode.Single);
    }
    public void LoadNextStage()
    {
        if (NextStage != null)
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(NextStage, LoadSceneMode.Single);
            SceneManager.UnloadSceneAsync("ResultScene");
        }
    }
}
