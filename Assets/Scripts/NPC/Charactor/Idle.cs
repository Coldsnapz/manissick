﻿using KS.Character;
using UnityEngine;
using UnityEngine.AI;

public class Idle : State
{
    private SimpleWaypointsAIFSM _simpleWaypointsAifsm;
    private NavMeshAgent agent;
    private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
    private AICharacterControlKSModified aiCharacterControlKsModified;
    private float idleTime = 0;
    public Idle(FiniteStateMachine fsm) : base(fsm)
    {
        _simpleWaypointsAifsm = ((SimpleWaypointsAIFSM)this.FSM);

        agent = ((SimpleWaypointsAIFSM)this.FSM).Agent;
        thirdPersonCharacterKsModified = ((SimpleWaypointsAIFSM)this.FSM).ThirdPersonChar;
        aiCharacterControlKsModified = ((SimpleWaypointsAIFSM)this.FSM).AICharControlKsModified;
    }

    public override void Enter()
    {
        if (agent != null)
        {
            agent.isStopped = true;
        }
        idleTime = Random.Range(3f, 10f);
        _simpleWaypointsAifsm.Anim.SetFloat("IdleValue", Random.Range(0, 4));
        _simpleWaypointsAifsm.Anim.SetBool("GoesIdle", true);
        base.Enter();
    }

    public override void Update()
    {
        if (idleTime > 0)
        {
            idleTime -= Time.deltaTime;
        }
        else
        {
            this.FSM.NextState = new Patrol(this.FSM);
            this.StateStage = StateEvent.EXIT;
        }

        if (_simpleWaypointsAifsm._reaction)
        {
            this.FSM.NextState = new Staring(this.FSM);
            this.StateStage = StateEvent.EXIT;
            _simpleWaypointsAifsm._reaction = false;
        }

        if (_simpleWaypointsAifsm.gameController.SUSPECTPOINT <= 25)
        {
            if (_simpleWaypointsAifsm._IsGuard)
            {
                this.FSM.NextState = new Chasing(this.FSM);
                this.StateStage = StateEvent.EXIT;
                _simpleWaypointsAifsm._reaction = false;
            }
        }
    }

    public override void Exit()
    {
        if (agent != null)
        {
            agent.isStopped = false;
        }
        _simpleWaypointsAifsm.Anim.SetBool("GoesIdle", false);
        base.Exit();
    }
}
