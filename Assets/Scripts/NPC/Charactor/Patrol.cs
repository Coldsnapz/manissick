﻿using KS.Character;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : State
{
    private SimpleWaypointsAIFSM _simpleWaypointsAifsm;

    private int currentWaypointIdx = -1;

    private NavMeshAgent agent;
    private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
    private AICharacterControlKSModified aiCharacterControlKsModified;

    public Patrol(FiniteStateMachine fsm) : base(fsm)
    {
        _simpleWaypointsAifsm = ((SimpleWaypointsAIFSM)this.FSM);

        agent = ((SimpleWaypointsAIFSM)this.FSM).Agent;
        thirdPersonCharacterKsModified = ((SimpleWaypointsAIFSM)this.FSM).ThirdPersonChar;
        aiCharacterControlKsModified = ((SimpleWaypointsAIFSM)this.FSM).AICharControlKsModified;
    }

    public override void Enter()
    {
        if (_simpleWaypointsAifsm._IsGuard && _simpleWaypointsAifsm._IsNotMoving)
        {
            if (_simpleWaypointsAifsm.wayPoints[0] != null)
            {
                aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.wayPoints[0]);
            }
            else
            {
                Debug.LogWarning(_simpleWaypointsAifsm.gameObject.name + " has no returning traget.");
            }
        }
        if (!_simpleWaypointsAifsm._IsNotMoving)
        {
            float lastDist = Mathf.Infinity; // Store distance between NPC and waypoints.
            // Calculate closest waypoint by looping around each one and calculating the distance between the NPC and each waypoint.
            for (int i = 0; i < _simpleWaypointsAifsm.wayPoints.Count; i++)
            {
                GameObject thisWP = _simpleWaypointsAifsm.wayPoints[i].gameObject;
                float distance = Vector3.Distance(_simpleWaypointsAifsm.Npc.transform.position, thisWP.transform.position);
                if (distance < lastDist)
                {
                    currentWaypointIdx = i;
                    lastDist = distance;

                    _simpleWaypointsAifsm.AICharControlKsModified.SetTarget(thisWP.transform);
                }
            }

            currentWaypointIdx = Random.Range(0, _simpleWaypointsAifsm.wayPoints.Count);

            aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.wayPoints[currentWaypointIdx]);
            agent.SetDestination(aiCharacterControlKsModified.target.position);

            thirdPersonCharacterKsModified.Move(Vector3.zero, false, false);
        }

        //Proceed to the next stage of the FSM
        base.Enter();
    }

    public override void Update()
    {
        if (!_simpleWaypointsAifsm._IsNotMoving)
        {
            if (!_simpleWaypointsAifsm._getBlocked)
            {
                agent.isStopped = false;
                if (aiCharacterControlKsModified.target != null)
                    agent.SetDestination(aiCharacterControlKsModified.target.position);

                if (agent.remainingDistance > agent.stoppingDistance)
                {
                    //Move the agent
                    thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
                }
                else
                {
                    if (Random.Range(0, 100) < 50)
                    {
                        //increase waypoint index
                        currentWaypointIdx = Random.Range(0, _simpleWaypointsAifsm.wayPoints.Count);
                        //Debug.Log(currentWaypointIdx + ", " +Time.time);


                        //Set target to the next waypoint
                        aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.wayPoints[currentWaypointIdx]);
                        agent.SetDestination(aiCharacterControlKsModified.target.position);

                        //Stop the character movement
                        thirdPersonCharacterKsModified.Move(Vector3.zero, false, false);
                    }
                    else
                    {
                        this.FSM.NextState = new Idle(this.FSM);
                        this.StateStage = StateEvent.EXIT;
                        _simpleWaypointsAifsm._reaction = false;
                    }
                }
            }
            else
            {
                agent.isStopped = true;
            }
        }
        else
        {
            if (!_simpleWaypointsAifsm._getBlocked)
            {
                if (_simpleWaypointsAifsm.gameController._GuardStandbyAtArea)
                {
                    if (aiCharacterControlKsModified.target != null)
                    {
                        agent.SetDestination(aiCharacterControlKsModified.target.position);
                    }

                    if (agent.remainingDistance > agent.stoppingDistance)
                    {
                        thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
                    }
                    else
                    {
                        this.FSM.NextState = new Idle(this.FSM);
                        this.StateStage = StateEvent.EXIT;
                        _simpleWaypointsAifsm._reaction = false;
                    }
                }
                else
                {
                    Debug.Log("AFK");
                    this.FSM.NextState = new Idle(this.FSM);
                    this.StateStage = StateEvent.EXIT;
                    _simpleWaypointsAifsm._reaction = false;
                }
            }
            else
            {
                agent.isStopped = true;
            }
        }

        if (_simpleWaypointsAifsm._reaction)
        {
            this.FSM.NextState = new Staring(this.FSM);
            this.StateStage = StateEvent.EXIT;
            _simpleWaypointsAifsm._reaction = false;
        }

        if (_simpleWaypointsAifsm.gameController.SUSPECTPOINT <= 25)
        {
            if (_simpleWaypointsAifsm._IsGuard)
            {
                this.FSM.NextState = new Chasing(this.FSM);
                this.StateStage = StateEvent.EXIT;
                _simpleWaypointsAifsm._reaction = false;
            }
        }
    }

    public override void Exit()
    {
        base.Exit();
    }
}
