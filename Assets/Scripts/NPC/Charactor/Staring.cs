﻿using KS.Character;
using UnityEngine;
using UnityEngine.AI;

public class Staring : State
{
    private SimpleWaypointsAIFSM _simpleWaypointsAifsm;

    private int currentWaypointIdx = -1;

    private NavMeshAgent agent;
    private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
    private AICharacterControlKSModified aiCharacterControlKsModified;
    private float NextReaction = 0;
    private float CoolDownReaction = 4;
    private float CountdownForTurning = 4;
    
    

    public Staring(FiniteStateMachine fsm) : base(fsm)
    {
        _simpleWaypointsAifsm = ((SimpleWaypointsAIFSM)this.FSM);
        agent = ((SimpleWaypointsAIFSM)this.FSM).Agent;
        thirdPersonCharacterKsModified = ((SimpleWaypointsAIFSM)this.FSM).ThirdPersonChar;
        aiCharacterControlKsModified = ((SimpleWaypointsAIFSM)this.FSM).AICharControlKsModified;

    }

    public override void Enter()
    {
        if (_simpleWaypointsAifsm._IsDoctor)
        {
            _simpleWaypointsAifsm.gameController.SUSPECTPOINT -= 10;
        }
        if (_simpleWaypointsAifsm._IsGuard)
        {
            _simpleWaypointsAifsm.gameController.SUSPECTPOINT -= 5;
        }
        NextReaction = Time.time + CoolDownReaction;
        Debug.Log("HEY");
        _simpleWaypointsAifsm.soundSfx.PlayOneShot(_simpleWaypointsAifsm.yell);
        
        agent.isStopped = true;
        Vector3 playerPos = _simpleWaypointsAifsm.player.transform.position;
        Vector3 turretPos = _simpleWaypointsAifsm.Npc.transform.position;

        Quaternion lookatPlayer = Quaternion.LookRotation(playerPos - turretPos);
        lookatPlayer.x = 0;
        lookatPlayer.z = 0;

        Vector3 direction = playerPos - turretPos; // Provides the vector from the NPC to the player.
        float angle = Vector3.Angle(direction, _simpleWaypointsAifsm.Npc.transform.forward); // Provide angle of sight.

        //Rotate toward the dected player
        _simpleWaypointsAifsm.Npc.transform.rotation = Quaternion.Slerp(_simpleWaypointsAifsm.Npc.transform.rotation, lookatPlayer, _simpleWaypointsAifsm.RotSpeed * Time.deltaTime);

        _simpleWaypointsAifsm.Anim.SetTrigger("Reacting");

        base.Enter();
    }

    public override void Update()
    {
        if (_simpleWaypointsAifsm._reaction)
        {
            NextReaction = Time.time + CoolDownReaction;
            _simpleWaypointsAifsm._reaction = false;
        }

        if (CountdownForTurning > 0)
        {
            CountdownForTurning -= Time.deltaTime;
        }
        else
        {
            Vector3 playerPos = _simpleWaypointsAifsm.player.transform.position;
            Vector3 turretPos = _simpleWaypointsAifsm.Npc.transform.position;

            Quaternion lookatPlayer = Quaternion.LookRotation(playerPos - turretPos);
            lookatPlayer.x = 0;
            lookatPlayer.z = 0;

            Vector3 direction = playerPos - turretPos; // Provides the vector from the NPC to the player.
            float angle = Vector3.Angle(direction, _simpleWaypointsAifsm.Npc.transform.forward); // Provide angle of sight.

            //Rotate toward the dected player
            _simpleWaypointsAifsm.Npc.transform.rotation = Quaternion.Slerp(_simpleWaypointsAifsm.Npc.transform.rotation, lookatPlayer, _simpleWaypointsAifsm.RotSpeed * Time.deltaTime);

        }

        if (Time.time > NextReaction)
        {
            this.FSM.NextState = new Patrol(this.FSM);
            this.StateStage = StateEvent.EXIT;
            _simpleWaypointsAifsm._reaction = false;
            agent.isStopped = false;
            Debug.Log("Hmmmm");
        }
        if (_simpleWaypointsAifsm.gameController.SUSPECTPOINT <= 30)
        {
            if (_simpleWaypointsAifsm._IsGuard)
            {
                this.FSM.NextState = new Chasing(this.FSM);
                this.StateStage = StateEvent.EXIT;
                _simpleWaypointsAifsm._reaction = false;
            }
        }
    }

    public override void Exit()
    {
        _simpleWaypointsAifsm.Anim.ResetTrigger("Reacting");
        base.Exit();
    }
}
