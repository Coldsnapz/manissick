﻿using KS.Character;
using UnityEngine;
using UnityEngine.AI;

public class Chasing : State
{
    private SimpleWaypointsAIFSM _simpleWaypointsAifsm;
    private NavMeshAgent agent;
    private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
    private AICharacterControlKSModified aiCharacterControlKsModified;
    private float TimeToReturn;

    public Chasing(FiniteStateMachine fsm) : base(fsm)
    {
        _simpleWaypointsAifsm = ((SimpleWaypointsAIFSM)this.FSM);
        agent = ((SimpleWaypointsAIFSM)this.FSM).Agent;
        thirdPersonCharacterKsModified = ((SimpleWaypointsAIFSM)this.FSM).ThirdPersonChar;
        aiCharacterControlKsModified = ((SimpleWaypointsAIFSM)this.FSM).AICharControlKsModified;
    }

    public override void Enter()
    {
        TimeToReturn = 15;
        agent.isStopped = false;
        _simpleWaypointsAifsm._Chasing = true;
        _simpleWaypointsAifsm.gameController.chasedby++;
        aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.player.transform);
        agent.SetDestination(aiCharacterControlKsModified.target.position);
        thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
        base.Enter();
    }

    public override void Update()
    {

        if (_simpleWaypointsAifsm.CanSeePlayer())
        {
            if (TimeToReturn < 10)
            {
                TimeToReturn = 10;
            }
            agent.speed = 0.8f;
            aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.player.transform);
            agent.SetDestination(aiCharacterControlKsModified.target.position);
            thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            if (TimeToReturn > 0)
            {
                if (TimeToReturn < 5)
                {
                    agent.speed = 0.6f;
                }
                else { agent.speed = 0.8f; }
                TimeToReturn -= Time.deltaTime;

                aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.player.transform);
                agent.SetDestination(aiCharacterControlKsModified.target.position);
                thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
            }
            else
            {
                this.FSM.NextState = new Idle(this.FSM);
                this.StateStage = StateEvent.EXIT;
            }
        }
        Debug.Log(agent.speed);
    }

    public override void Exit()
    {
        _simpleWaypointsAifsm.gameController.chasedby--;
        if (_simpleWaypointsAifsm._IsGuard && _simpleWaypointsAifsm._IsNotMoving)
        {
            if (_simpleWaypointsAifsm.wayPoints[0] != null)
            {
                aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.wayPoints[0]);
            }
            else
            {
                Debug.LogWarning(_simpleWaypointsAifsm.gameObject.name + " has no returning traget.");
            }
        }
        _simpleWaypointsAifsm._Chasing = false;
        agent.speed = 0.5f;
        _simpleWaypointsAifsm._reaction = false;
        base.Exit();
    }
}
