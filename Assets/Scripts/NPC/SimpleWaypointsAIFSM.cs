﻿using System.Collections.Generic;
using KS.Character;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(UnityEngine.Animator))]
[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class SimpleWaypointsAIFSM : FiniteStateMachine
{
    public AudioSource soundSfx{ get; set; }
    public AudioClip yell;
    public List<Transform> wayPoints;

    public GameObject Npc { get; set; }
    public Animator Anim { get; set; }

    [SerializeField] public Transform player;
    public Transform Player
    {
        get { return player; }
    }
    public NavMeshAgent Agent { get; set; }

    public ThirdPersonCharacterKSModified ThirdPersonChar { get; set; }
    public AICharacterControlKSModified AICharControlKsModified { get; set; }
    [SerializeField] NPCClass NpcClass;

    public bool _reaction { get; set; } = false;
    public bool _wieldReaction { get; set; } = false;
    public bool _IsNotMoving { get; set; }
    public bool _IsGuard { get; set; }
    public bool _IsDoctor { get; set; }
    public bool _getBlocked { get; set; }
    public float RotSpeed { get; set; } = 0;
    public bool _Chasing { get; set; } = false;

    public GameController gameController { get; set; }

    private void Start()
    {
        _IsNotMoving = NpcClass._IsNotMoving;
        _IsGuard = NpcClass._IsGuard;
        _IsDoctor = NpcClass._IsDoctor;
        _getBlocked = false;
        gameController = GameObject.FindObjectOfType<GameController>();
        CurrentState = new Idle(this);
        Npc = GetComponent<Transform>().gameObject;
        Anim = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        _reaction = false;
        ThirdPersonChar = GetComponent<ThirdPersonCharacterKSModified>();
        AICharControlKsModified = GetComponent<AICharacterControlKSModified>();
        RotSpeed = Agent.angularSpeed;
        soundSfx=GetComponent<AudioSource>();
    }
    public bool CanSeePlayer()
    {
        Vector3 direction = player.position - Npc.transform.position; // Provides the vector from the NPC to the player.
        float angle = Vector3.Angle(direction, Npc.transform.forward); // Provide angle of sight.

        // If player is close enough to the NPC AND within the visible viewing angle...
        if (direction.magnitude < 5f && angle < 30)
        {
            return true; // NPC CAN see the player.
        }
        if ((player.position - Npc.transform.position).magnitude < 3)
        {
            return true; // NPC CAN see the player.
        }
        return false; // NPC CANNOT see the player.
    }
}





