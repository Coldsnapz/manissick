﻿using UnityEngine;
[CreateAssetMenu(fileName = "NPCClass", menuName = "ScriptableObjects/MyScriptableObject", order = 1)]
public class NPCClass : ScriptableObject
{
    [SerializeField] public bool _IsNotMoving;
    [SerializeField] public bool _IsGuard;
    [SerializeField] public bool _IsDoctor;
}
