﻿using UnityEngine;


[RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof (ControlScript))]
public class AIControlScript : MonoBehaviour
{

     public UnityEngine.AI.NavMeshAgent agent { get; private set; }
     public ControlScript character { get; private set; } 
     public Transform target; 
    // Start is called before the first frame update
    void Start()
    {
         agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ControlScript>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
    }

    // Update is called once per frame
    void Update()
    {
        // if (target != null)
             //   agent.SetDestination(target.position);

           // if (agent.remainingDistance > agent.stoppingDistance)
            {
          //      character.Move(agent.desiredVelocity, false, false);
            }
         //   else
         //   {
          //      character.Move(Vector3.zero, false, false);
         //       target = null;
         //   }

            
    }
     public void SetTarget(Transform target)
        {
            this.target = target;
        }
}
