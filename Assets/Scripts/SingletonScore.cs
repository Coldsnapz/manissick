﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SingletonScore : Singleton<SingletonScore>
{
    public bool Result { get; set; }
    public float Score { get; set; }
    public string Object { get; set; }
    public string Human { get; set; }
    public string TimeLeft { get; set; }
    public float Suspect { get; set; }
    public string CurrentStage { get; set; }
    public string NextStage { get; set; }
    public bool Stage2 { get; set; }
    public bool Stage3 { get; set; }
    public void ResetScore()
    {
        Result = false;
        Score = 0;
        Object = null;
        Human = null;
        TimeLeft = null;
        Suspect = 100;
        CurrentStage = null;
        NextStage = null;
    }
    public void SaveFile()
    {
        string destination = Application.persistentDataPath + "/save.dat";
        Debug.Log("Saved file at "+Application.persistentDataPath + "/save.dat");
        FileStream file;

        if (File.Exists(destination)) file = File.OpenWrite(destination);
        else file = File.Create(destination);

        GameData data = new GameData(Stage2, Stage3);
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, data);
        file.Close();
    }
    public void LoadFile()
    {
        string destination = Application.persistentDataPath + "/save.dat";
        Debug.Log("loaded file from "+Application.persistentDataPath + "/save.dat");
        FileStream file;

        if (File.Exists(destination)) file = File.OpenRead(destination);
        else
        {
            Debug.LogError("File not found");
            return;
        }

        BinaryFormatter bf = new BinaryFormatter();
        GameData data = (GameData)bf.Deserialize(file);
        file.Close();

        Stage2 = data.Stage2ulk;
        Stage3 = data.Stage3ulk;
    }
}
[System.Serializable]
public class GameData
{
    public bool Stage2ulk { get; set; }
    public bool Stage3ulk { get; set; }

    public GameData(bool st2ulk, bool st3ulk)
    {
        Stage2ulk = st2ulk;
        Stage3ulk = st3ulk;
    }
}
