﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeThroughObject : MonoBehaviour {
    private Renderer m_renderer;
    private void Start () {
        m_renderer = gameObject.GetComponent<Renderer> ();
    }

    public void SetTransparent () {
        foreach (Material m in m_renderer.materials) {
            m.SetFloat ("_Mode", 2);
            m.SetInt ("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
            m.SetInt ("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            m.SetInt ("_ZWrite", 0);
            m.DisableKeyword ("_ALPHATEST_ON");
            m.EnableKeyword ("_ALPHABLEND_ON");
            m.DisableKeyword ("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = 3000;
        }
    }
    public void SetOpaque () {
        Invoke ("SetBack ()", .5f);
    }
    public void SetBack () {
        foreach (Material m in m_renderer.materials) {
            m.SetInt ("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
            m.SetInt ("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
            m.SetInt ("_ZWrite", 1);
            m.DisableKeyword ("_ALPHATEST_ON");
            m.DisableKeyword ("_ALPHABLEND_ON");
            m.DisableKeyword ("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = -1;
        }
    }
}