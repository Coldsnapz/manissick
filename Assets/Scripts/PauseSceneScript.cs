﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseSceneScript : MonoBehaviour
{
    private void Start()
    {
        Cursor.visible = true;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnloadPause();
        }
    }
    public void UnloadPause()
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        SceneManager.UnloadSceneAsync("PauseScene");
    }

    public void LoadStageSelect()
    {
        Time.timeScale = 1;
        SceneManagementSingleton.Instance.isQuitingStage = true;
        SceneManager.UnloadSceneAsync("PauseScene");
        SceneManager.LoadScene("SceneMainMenuBackground", LoadSceneMode.Single);
    }
}
