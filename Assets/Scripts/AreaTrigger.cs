﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour
{
    GameController gameController;
    private void Awake()
    {
        gameController = GameObject.FindObjectOfType<GameController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Exit"))
        {
            if (gameController._enterArea)
            {
                Debug.Log("Escaped");
                gameController.EscapeArea();
            }
        }
        if (other.gameObject.CompareTag("Entrance"))
        {
            Debug.Log("EnterArea");
            gameController.AreaEnter();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("SuspectZone"))
        {
            Debug.Log("Suspect");
            gameController.SUSPECTPOINT -= Time.deltaTime * 50;
            gameController.RegenerationCD = 0;
            gameController.InSuspectZone = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Exit"))
        {
            if (gameController._enterArea)
            {
                Debug.Log("Escaped");
                gameController.EscapeArea();
            }
        }
        if (other.gameObject.CompareTag("SuspectZone"))
        {
            gameController.InSuspectZone = false;
        }
    }
}
