﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class infectableHuman : MonoBehaviour
{
    private bool _isInfected = false;

    public bool IsInfected
    {
        get { return _isInfected; }
        set { _isInfected = value; }
    }

    GameController gameController;
    SimpleWaypointsAIFSM simpleWaypoints;
    private void Start()
    {
        gameController = GameObject.FindObjectOfType<GameController>();
        gameController._MaxinfectableHuman += 1;
        simpleWaypoints = gameObject.transform.parent.gameObject.GetComponent<SimpleWaypointsAIFSM>();
    }

    public void Infected()
    {
        simpleWaypoints._reaction = true;
        if (simpleWaypoints._IsDoctor)
        {
            gameController.SUSPECTPOINT -= 15;
        }
        gameController.SUSPECTPOINT -= 10;
        gameController.RegenerationCD = 0;
        if (!_isInfected)
        {
            this.GetComponent<ParticleSystem>().Play();
            Debug.Log(this.name + " is now infected!");
            _isInfected = true;
            gameController.INFECTEDHUMAN += 1;
        }
    }
    public void InfectedByTouch()
    {
        simpleWaypoints._reaction = true;
        if (simpleWaypoints._IsDoctor)
        {
            gameController.SUSPECTPOINT -= 20;
        }
        gameController.SUSPECTPOINT -= 30;
        gameController.RegenerationCD = 0;
        if (!_isInfected)
        {
            this.GetComponent<ParticleSystem>().Play();
            Debug.Log(this.name + " is now infected!");
            _isInfected = true;
            gameController.INFECTEDHUMAN += 1;
        }
    }

    public void Cure()
    {
        if (_isInfected)
        {
            this.GetComponent<ParticleSystem>().Stop();
            Debug.Log(this.name + " is now cured!");
            _isInfected = false;
            gameController.INFECTEDHUMAN -= 1;
        }
    }
}
