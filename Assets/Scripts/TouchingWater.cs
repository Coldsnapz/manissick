﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

namespace Invector.vCharacterController
{
    public class TouchingWater : MonoBehaviour
    {
        [SerializeField] vThirdPersonMotor playerController = null;
        [SerializeField] float _inWaterSpeed = 0.75f;
        GameController gameController;

        private void Start()
        {
            gameController = GameObject.FindObjectOfType<GameController>();
        }
        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.CompareTag("WaterTrigger"))
            {
                gameController.SUSPECTPOINT -= Time.deltaTime * 1f;
                playerController.SpeedFactor = _inWaterSpeed;
                GetAttendtion();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("WaterTrigger"))
            { playerController.SpeedFactor = 1f; }
        }

        private void GetAttendtion()
        {
            infectableHuman[] infectableHumanAround = GameObject.FindObjectsOfType<infectableHuman>();
            foreach (infectableHuman a in infectableHumanAround)
            {
                GameObject aIsAIFSM = a.gameObject.transform.parent.gameObject;
                if ((this.gameObject.transform.position - aIsAIFSM.transform.position).magnitude < 15f)
                {
                    gameController.SUSPECTPOINT -= Time.deltaTime * 5f;
                    gameController.RegenerationCD = 0;
                    aIsAIFSM.GetComponent<SimpleWaypointsAIFSM>()._reaction = true;
                }
            }
        }
    }
}
