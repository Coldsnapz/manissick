﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneezeKey : MonoBehaviour
{
    
    public AudioClip cough;
    private float cooldownTime = 2;
    private float nextTime = 0;
    private bool isRolling = false;
    public bool IsRolling
    {
        get { return isRolling; }
        set { isRolling = value; }
    }
    GameController gameController;

    private void Start()
    {
        gameController = GameObject.FindObjectOfType<GameController>();
        
    }
    void Update()
    {
        if (Time.time > nextTime)
        {
            if (!isRolling)
                if (Input.GetKeyDown(KeyCode.J))
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().Play("Sneeze");
                    GameObject.Find("Sneeze").GetComponent<CapsuleCollider>().enabled = true;
                    GameObject.Find("Sneeze").GetComponent<ParticleSystem>().Play();
                    nextTime = Time.time + cooldownTime;
                    this.GetComponent<AudioSource>().PlayOneShot(cough);
                    GetAttendtion();

                }
        }

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Roll"))
        {
            isRolling = true;
        }
        else if (isRolling)
        {
            isRolling = false;
        }
    }
    private void GetAttendtion()
    {
        infectableHuman[] infectableHumanAround = GameObject.FindObjectsOfType<infectableHuman>();
        foreach (infectableHuman a in infectableHumanAround)
        {
            GameObject aIsAIFSM = a.gameObject.transform.parent.gameObject;
            if ((this.gameObject.transform.position - aIsAIFSM.transform.position).magnitude < 5f)
            {
                gameController.SUSPECTPOINT -= 10;
                gameController.RegenerationCD = 0;
                aIsAIFSM.GetComponent<SimpleWaypointsAIFSM>()._reaction = true;
            }
        }
    }
}
