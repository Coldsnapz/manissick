﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingTrigger : MonoBehaviour
{
    SimpleWaypointsAIFSM _simpleWaypointsAIFSM;
    GameController _gameController;
    void Start()
    {
        _simpleWaypointsAIFSM = gameObject.transform.parent.gameObject.GetComponent<SimpleWaypointsAIFSM>();
        _gameController = GameObject.FindObjectOfType<GameController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _simpleWaypointsAIFSM._getBlocked = true;
            if (_simpleWaypointsAIFSM._Chasing)
            {
                _gameController.Lose();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _simpleWaypointsAIFSM._getBlocked = false;
        }
    }
}
